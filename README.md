# ics-ans-role-jmasar

Ansible role to install [jmasar-service](https://gitlab.esss.lu.se/ics-software/jmasar-service).

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

See: [Default variables](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jmasar
```

## License

BSD 2-clause
