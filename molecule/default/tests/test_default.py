import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name", ["jmasar.service", "nginx"])
def test_service_enabled_and_running(host, name):
    service = host.service(name)
    assert service.is_enabled
    assert service.is_running


def test_elasticsearch_running(host):
    with host.sudo():
        elasticsearch = host.docker("jmasar_elasticsearch")
        assert elasticsearch.is_running
